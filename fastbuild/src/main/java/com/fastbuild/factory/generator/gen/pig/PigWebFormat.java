package com.fastbuild.factory.generator.gen.pig;

import com.fastbuild.common.utils.StringUtils;
import com.fastbuild.factory.generator.common.FactoryConst;
import com.fastbuild.factory.generator.common.FileFormatter;
import com.fastbuild.factory.generator.domain.AppConfig;
import com.fastbuild.factory.generator.gen.AbstractFormat;
import com.fastbuild.factory.generator.gen.ruoyi.RuoyiCloudRequestPathFormat;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.FileFilterUtils;

import java.io.File;
import java.io.IOException;

public class PigWebFormat extends AbstractFormat {

    private final String GEN_ID = "pig#web";

    public PigWebFormat(AppConfig app) {
        super(app);
    }

    @Override
    protected String getGenId() {
        return GEN_ID;
    }

    @Override
    protected boolean validate() {
        return FactoryConst.app.PIG.equals(app.getAppId())
                && !FactoryConst.web.THYMELEAF.equals(project.getWebFramework());
    }

    @Override
    protected void dependency() throws Exception {
    }

    @Override
    protected void fileGenerator() throws Exception {
        String srcPath = properties.getFactoryPigWebPath();
        File srcFile = new File(srcPath);

        String destPath = project.getWorkPath() + File.separator + project.getUiName();
        File destRoot = new File(destPath);

        FileUtils.copyDirectory(srcFile, destRoot, FileFilterUtils.trueFileFilter());
    }
}
