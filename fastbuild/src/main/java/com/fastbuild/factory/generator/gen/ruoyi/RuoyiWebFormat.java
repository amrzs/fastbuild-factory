package com.fastbuild.factory.generator.gen.ruoyi;

import com.fastbuild.common.utils.StringUtils;
import com.fastbuild.factory.generator.common.FactoryConst;
import com.fastbuild.factory.generator.common.FileFormatter;
import com.fastbuild.factory.generator.domain.AppConfig;
import com.fastbuild.factory.generator.gen.AbstractFormat;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.FileFilterUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class RuoyiWebFormat extends AbstractFormat {

    private final String GEN_ID = "ruoyi#web";

    public RuoyiWebFormat(AppConfig app) {
        super(app);
    }

    @Override
    protected String getGenId() {
        return GEN_ID;
    }

    @Override
    protected boolean validate() {
        return FactoryConst.app.RUOYI.equals(app.getAppId())
                && !FactoryConst.web.THYMELEAF.equals(project.getWebFramework());
    }

    @Override
    protected void dependency() throws Exception {
    }

    @Override
    protected void fileGenerator() throws Exception {
        String srcPath = this.getSrcPath();
        if (StringUtils.isNotEmpty(srcPath)) {
            File srcFile = new File(srcPath);

            String destPath = project.getWorkPath() + File.separator + project.getUiName();
            File destRoot = new File(destPath);
            List<String> exclude = this.getExcludeFile();
            FileUtils.copyDirectory(srcFile, destRoot, file -> !exclude.contains(file.getName()));

            this.fileContentFormat(destRoot);
        }
    }

    private List<String> getExcludeFile () {
        List<String> exclude = new ArrayList<>();
        exclude.add("node_modules");
        exclude.add(".DS_Store");
        exclude.add(".idea");
        return exclude;
    }

    private void fileContentFormat (File destRoot) throws IOException {
        FileFormatter vueFormatter = new FileFormatter(destRoot, FileFilterUtils.suffixFileFilter("vue"));
        vueFormatter.replaceAll("若依管理系统", project.getProjectTitle());
        vueFormatter.replaceAll("若依后台管理系统", project.getProjectTitle());
        vueFormatter.format();

        FileFormatter developmentFormatter = new FileFormatter(destRoot, FileFilterUtils.suffixFileFilter("development"));
        developmentFormatter.replaceAll("若依管理系统", project.getProjectTitle());
        developmentFormatter.replaceAll("若依后台管理系统", project.getProjectTitle());
        developmentFormatter.format();

        FileFormatter productionFormatter = new FileFormatter(destRoot, FileFilterUtils.suffixFileFilter("production"));
        productionFormatter.replaceAll("若依管理系统", project.getProjectTitle());
        productionFormatter.replaceAll("若依后台管理系统", project.getProjectTitle());
        productionFormatter.format();

        // 适配微服务的请求地址转换
        RuoyiCloudRequestPathFormat cloudFormat = new RuoyiCloudRequestPathFormat(project, destRoot);
        cloudFormat.format();

    }

    private String getSrcPath () {
        if (FactoryConst.web.VUE2.equals(project.getWebFramework()) && FactoryConst.ui.ELEMENT.equals(project.getWebUI())) {
            if (FactoryConst.server.CLOUD.equals(project.getServerMode())) {
                return properties.getFactoryRuoyiCloudPath() + File.separator + "ruoyi-ui";
            }else {
                // vue2 + element
                return properties.getFactoryRuoyiVuePath() + File.separator + "ruoyi-ui";
            }
        } else if (FactoryConst.web.VUE3.equals(project.getWebFramework()) && FactoryConst.ui.ELEMENT.equals(project.getWebUI())) {
            // vue3 + element
            return properties.getFactoryRuoyiVue3Path();
        } else if (FactoryConst.web.VUE2.equals(project.getWebFramework()) && FactoryConst.ui.ANTD.equals(project.getWebUI())) {
            // vue2 + antd
            return properties.getFactoryRuoyiVueAntdPath();
        } else if (FactoryConst.web.VUE3.equals(project.getWebFramework()) && FactoryConst.ui.ANTD.equals(project.getWebUI())) {
            // vue3 + antd
            return properties.getFactoryRuoyiVue3AntdPath();
        } else if (FactoryConst.web.REACT.equals(project.getWebFramework()) && FactoryConst.ui.ANTD.equals(project.getWebUI())) {
            // react + antd
            return properties.getFactoryRuoyiReackAntdPath();
        }
        return null;
    }
}
