package com.fastbuild.factory.generator.gen.snowy;

import com.fastbuild.factory.generator.common.FactoryConst;
import com.fastbuild.factory.generator.domain.AppConfig;
import com.fastbuild.factory.generator.gen.AbstractFormat;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.FileFilterUtils;

import java.io.File;

public class SnowyWebFormat extends AbstractFormat {

    private final String GEN_ID = "snowy#web";

    public SnowyWebFormat(AppConfig app) {
        super(app);
    }

    @Override
    protected String getGenId() {
        return GEN_ID;
    }

    @Override
    protected boolean validate() {
        return FactoryConst.app.SNOWY.equals(app.getAppId());
    }

    @Override
    protected void dependency() throws Exception {
    }

    @Override
    protected void fileGenerator() throws Exception {
        String srcPath = this.getSrcPath("/snowy-admin-web");
        File srcFile = new File(srcPath);
        if (!srcFile.exists()) {
            srcPath = this.getSrcPath("/_web");
            srcFile = new File(srcPath);
        }

        String destPath = project.getWorkPath() + File.separator + project.getUiName();
        File destRoot = new File(destPath);

        FileUtils.copyDirectory(srcFile, destRoot, FileFilterUtils.trueFileFilter());
    }

    private String getSrcPath (String dirName) {
        if (FactoryConst.server.CLOUD.equals(this.project.getServerMode())) {
            return properties.getFactorySnowyCloudPath() + dirName;
        } else {
            return properties.getFactorySnowyVuePath() + dirName;
        }
    }

}
