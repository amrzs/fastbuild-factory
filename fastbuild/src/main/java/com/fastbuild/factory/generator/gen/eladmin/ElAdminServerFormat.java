package com.fastbuild.factory.generator.gen.eladmin;

import com.fastbuild.factory.generator.common.FactoryConst;
import com.fastbuild.factory.generator.common.FileFormatter;
import com.fastbuild.factory.generator.common.FileHelper;
import com.fastbuild.factory.generator.domain.AppConfig;
import com.fastbuild.factory.generator.gen.AbstractFormat;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.text.CaseUtils;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * eladmin单体服务端代码格式化
 *
 * @author fastbuild@163.com
 */
public class ElAdminServerFormat extends AbstractFormat {

    private final String GEN_ID = "eladmin#server";

    public ElAdminServerFormat(AppConfig app) {
        super(app);
    }

    @Override
    protected String getGenId() {
        return GEN_ID;
    }

    @Override
    protected boolean validate() {
        return FactoryConst.app.ELADMIN.equals(app.getAppId());
    }

    @Override
    protected void dependency() {}

    @Override
    protected void fileGenerator() throws Exception {
        String srcPath = properties.getFactoryElAdminServerPath();
        File srcFile = new File(srcPath);
        File destRoot = new File(project.getServerRootPath());

        List<String> exclude = this.getExcludeFile();
        Map<String, String> replaceDirMap = this.getReplaceDirMap();
        Map<String, String> replaceFileMap = this.getReplaceFileMap();

        FileHelper.copyDirectory(srcFile, destRoot, new FileFilter() {
            @Override
            public boolean accept(File file) {
                return !exclude.contains(file.getName());
            }
        }, replaceDirMap, replaceFileMap);

        this.fileContentFormat(destRoot);
    }

    /**
     * 格式化文件内容
     *
     * @param destRoot
     * @throws IOException
     */
    private void fileContentFormat (File destRoot) throws IOException {
        final String classNamePrefix = CaseUtils.toCamelCase(project.getProjectName(), true, new char[] { '-', '_' });
        final String varNamePrefix = CaseUtils.toCamelCase(project.getProjectName(), false, new char[] { '-', '_' });

        FileFormatter javaFormatter = new FileFormatter(destRoot, FileFilterUtils.suffixFileFilter("java"));
        javaFormatter.replaceAll("me.zhengjie", project.getPackagePrefix());
        javaFormatter.replaceAll("eladmin", classNamePrefix);
        javaFormatter.replaceAll("eladmin", varNamePrefix);
        javaFormatter.format();

        FileFormatter pomFormatter = new FileFormatter(destRoot, FileFilterUtils.suffixFileFilter("xml"));
        pomFormatter.replaceAll("me.zhengjie", project.getPackagePrefix());
        pomFormatter.replaceAll("eladmin", varNamePrefix);
        pomFormatter.format();

        FileFormatter ymlFormatter = new FileFormatter(destRoot, FileFilterUtils.suffixFileFilter("yml"));
        ymlFormatter.replaceAll("eladmin", varNamePrefix);
        ymlFormatter.format();

        FileFormatter vmFormatter = new FileFormatter(destRoot, FileFilterUtils.suffixFileFilter("vm"));
        vmFormatter.replaceAll("me.zhengjie", project.getPackagePrefix());
        vmFormatter.replaceAll("eladmin", varNamePrefix);
        vmFormatter.format();

        FileFormatter sqlFormatter = new FileFormatter(destRoot, FileFilterUtils.suffixFileFilter("sql"));
        sqlFormatter.replaceAll("eladmin", project.getProjectName());
        sqlFormatter.format();

        FileFormatter factoriesFormatter = new FileFormatter(destRoot, FileFilterUtils.suffixFileFilter("factories"));
        factoriesFormatter.replaceAll("me.zhengjie", project.getPackagePrefix());
        factoriesFormatter.format();
    }

    private List<String> getExcludeFile () {
        List<String> exclude = new ArrayList<>();
        exclude.add(".git");
        exclude.add(".github");
        exclude.add(".idea");
        return exclude;
    }

    private Map<String, String> getReplaceDirMap () {
        Map<String, String> replaceDirMap = new LinkedHashMap<>();
        replaceDirMap.put("eladmin-", project.getProjectName() + "-");
        replaceDirMap.put("src/main/java/me/zhengjie", "src/main/java/" + project.getPackagePrefix().replaceAll("\\.", "/"));
        return replaceDirMap;
    }

    private Map<String, String> getReplaceFileMap () {
        Map<String, String> replaceFileMap = new LinkedHashMap<>();
        return replaceFileMap;
    }

}
